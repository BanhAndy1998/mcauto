# MCAuto #

This program lets anyone start up your Minecraft server by connecting to your public ip address on port 3334 given that this program is running on your PC when they attempt to connect. The server automatically closes after 3 hours of the connection being established unless it is refreshed.

# How to use #

Ensure that port 3334 is opened in your firewall settings and also for your local ip in your router settings before running the program. Place mcauto.jar and mcauto.bat inside of your minecraft server's folder and run mcauto.bat. To start up your server, connect to your publicIP:3334 on either a web browser or on Minecraft itself. If you connected through a web browser, you should receive a no data response page if the connection was successful. The user can then connect to the port that your Minecraft server is running on and enjoy playing.

# IMPORTANT NOTES #

- The server will stay open for 3 hours and will close regardless of whether or not there are people currently connected to the Minecraft server. To refresh this, simply reconnect to port 3334. The server will close 3 hours after the most recent connection to port 3334 instead.

- Ensure that all users log off at least 5 minutes before the server automatically shuts down. This program is reliant on Minecraft's auto save to save the world and user inventories as it cannot call /save-all and /exit. Users might find their inventories wiped, rolled back, and or the Minecraft world to be rolled back to an earlier state if any changes are made to them 5 minutes before the automatic shutdown. This can be avoided by refreshing the 3 hour timer.
