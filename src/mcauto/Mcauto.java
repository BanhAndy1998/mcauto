package mcauto;
import java.time.*;
import static java.time.temporal.ChronoUnit.MINUTES;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Mcauto implements Runnable{
	static Process p;
	static ServerSocket sSocket;
	static LocalTime last;
	static LocalTime curr;
	static ExecutorService executor = Executors.newFixedThreadPool(1);
	
	public static void main(String[] args) throws IOException {
		sSocket = new ServerSocket(3334);
		Thread t = new Thread(new Mcauto());
		last = LocalTime.now();
		curr = LocalTime.now();
		Socket s;
		while(true) {
			try {
				s = sSocket.accept();
				last = curr;
				curr = LocalTime.now();
				Thread.sleep(500);
				if(!s.isConnected())
					continue;
				if(!t.isAlive()) {
					System.out.println("connect");
					p = Runtime.getRuntime().exec("java -Xmx16392M -Xms16392M -jar server.jar nogui");
					try {
						t.start();
					}
					catch(IllegalThreadStateException e) {
						t = new Thread(new Mcauto());
						t.start();
					}
				}
				s.close();		
			} catch (IOException | InterruptedException e) {
			}
		}
	}
	
	public void run() {
		boolean threeHours = true;
		while(threeHours) {
			curr = LocalTime.now();
			if(MINUTES.between(last,curr) > 180) 
				threeHours = false;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
		}
		System.out.println("Server Closed");
		OutputStream os = p.getOutputStream();
		try {
			os.write("stop".getBytes());
		} catch (IOException e) {
		}
		p.destroy();
	}
}
